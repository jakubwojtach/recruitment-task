![Promo.com](https://ak03-promo-cdn.slidely.com/images/promoVideos/logo.svg)

# Recruitment Task

Project for a recruitment process of Promo(Slidely).

### How to run it:

1. Clone repository
2. Go to `simple-editor` folder
3. Install dependencies with `npm install/yarn install`
4. Run application with command `npm start/yarn start`

---

### Features:

1. Backgrounds Section
2. Editor
3. Enhancements Section
   3.1 Logo Picker
   3.2 Text Container
4. History Module

**1) Backgrounds Section**
a) At start we are getting list of 4 images with whole objects returned from the API, saved into the store and placed as the list elements.
b) We have a search bar, which allows us to look for a images based on a query.
c) We are setting image on the editor background, and have a delete button visible only if there is a possibility to do so.

**2) Editor**
a) We have download button, which allows us to save an image with all items that were added inside.
b) Items inside of the editor could be resided, moved with DnD only around the content of the editor, could be removed with right click and delete button inside of the menu.
c) We can download image only if we have any of those: - have any background inside - have any text inside - have any logo inside
d) Data is prepared as a canvas, exported into it with the usage of the library named Konva.

**3.1) Logo Picker**
a) We could add logo into the editor - haven't done adding by DnD, but on click of element.
b) We could have multiple logos
c) We could have multiple logos of the same instance
d) Added logos could be resized - max 150px x 150px - min 30px x 30px

**3.2) Text Container**
a) We could add text into the editor, with picking the font at first.
b) After we add text we could change font, change color and add styles.
c) Text could be moved only around the boundaries.

**4. History Module**
a) We have a possibilty to undo/redo history
b) We could clear whole history and content of the editor by clicking on reset

### Tech Stack:

1. React.js + Redux - view + state management combined
2. Redux-thunk - for async actions
3. Axios - for promisified calls into the services
4. Formsy - handling of the forms
5. Konva - for canvas handling
6. Yarn - for me personally better and faster NPM, because of caching.
7. Styled components - CSS in JS basically, i could have done it with SASS, but i thought that it would be nice to use.
8. Prop types - for typing, just that.

`Side note - i know that design looks a bit retro - that is intentional :)`
