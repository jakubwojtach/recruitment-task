import {
  CLEAR_BACKGROUND_IMAGE,
  PICK_BACKGROUND_IMAGE,
  SET_BACKGROUND_TEXT,
  CLEAR_BACKGROUND_TEXT,
  CLEAR_STATE,
} from '../Constants/actions';

export const setBackgroundUrl = url => dispatch => {
  dispatch({
    type: PICK_BACKGROUND_IMAGE,
    payload: url,
  });
};

export const clearBackgroundUrl = () => dispatch => {
  dispatch({
    type: CLEAR_BACKGROUND_IMAGE,
  });
};

export const setOverlayText = text => dispatch => {
  dispatch({
    type: SET_BACKGROUND_TEXT,
    payload: text,
  });
};

export const clearOverlayText = () => dispatch => {
  dispatch({
    type: CLEAR_BACKGROUND_TEXT,
  });
};

export const clearState = () => dispatch => {
  dispatch({
    type: CLEAR_STATE,
  });
};
