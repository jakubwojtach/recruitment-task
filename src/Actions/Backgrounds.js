import { searchForImages, getRandomImages } from '../Services/Images';

import {
  FETCH_IMAGES_REQUEST,
  FETCH_IMAGES_SUCCESS,
  FETCH_IMAGES_ERROR,
  SEARCH_IMAGES_REQUEST,
  SEARCH_IMAGES_SUCCESS,
  SEARCH_IMAGES_ERROR,
  SET_BACKGROUND_SEARCH_QUERY,
} from '../Constants/actions';

export const fetchImages = () => dispatch =>
  new Promise((resolve, reject) => {
    dispatch({
      type: FETCH_IMAGES_REQUEST,
    });
    getRandomImages()
      .then(response => {
        const { data } = response;
        dispatch({
          type: FETCH_IMAGES_SUCCESS,
          payload: data,
        });
        resolve();
      })
      .catch(error => {
        dispatch({
          type: FETCH_IMAGES_ERROR,
          payload: error,
          error: true,
        });
        reject();
      });
  });

export const fetchImagesByQuery = query => dispatch =>
  new Promise((resolve, reject) => {
    dispatch({
      type: SEARCH_IMAGES_REQUEST,
    });
    searchForImages(query)
      .then(response => {
        const { data } = response;
        dispatch({
          type: SEARCH_IMAGES_SUCCESS,
          payload: data,
        });
        resolve();
      })
      .catch(error => {
        dispatch({
          type: SEARCH_IMAGES_ERROR,
          payload: error,
          error: true,
        });
        reject();
      });
  });

export const setQuery = query => dispatch => {
  dispatch({
    type: SET_BACKGROUND_SEARCH_QUERY,
    payload: query,
  });
};
