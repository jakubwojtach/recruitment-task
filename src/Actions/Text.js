import {
  CHANGE_TEXT_BOLD,
  CHANGE_TEXT_UNDERLINE,
  CHANGE_TEXT_ITALIC,
  SET_TEXT_FONT,
  SET_TEXT_COLOR,
  SET_TEXT_VALUE,
} from '../Constants/actions';

export const setTextFont = font => dispatch => {
  dispatch({
    type: SET_TEXT_FONT,
    payload: font,
  });
};

export const setTextColor = color => dispatch => {
  dispatch({
    type: SET_TEXT_COLOR,
    payload: color,
  });
};

export const changeTextBold = () => dispatch => {
  dispatch({
    type: CHANGE_TEXT_BOLD,
  });
};

export const changeTextUndeline = () => dispatch => {
  dispatch({
    type: CHANGE_TEXT_UNDERLINE,
  });
};

export const changeTextItalic = () => dispatch => {
  dispatch({
    type: CHANGE_TEXT_ITALIC,
  });
};

export const setTextValue = text => dispatch => {
  dispatch({
    type: SET_TEXT_VALUE,
    payload: text,
  });
};
