import { ADD_LOGO, REMOVE_LOGO, UPDATE_LOGO } from '../Constants/actions';

export const addLogo = data => dispatch => {
  dispatch({
    type: ADD_LOGO,
    payload: data,
  });
};

export const removeLogo = name => dispatch => {
  dispatch({
    type: REMOVE_LOGO,
    payload: name,
  });
};

export const updateLogo = data => dispatch => {
  dispatch({
    type: UPDATE_LOGO,
    payload: data,
  });
};
