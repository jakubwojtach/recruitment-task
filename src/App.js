import React, { Component, Fragment } from 'react';
import Backgrounds from './Components/Backgrounds';
import Editor from './Components/Editor';
import Text from './Components/Text';
import LogoPicker from './Components/LogoPicker';
import Controls from './Components/Controls';

import styled, { createGlobalStyle, ThemeProvider } from 'styled-components';

const theme = {
  contSize: '1200px',
  fontSize: '16px',
  grey: '#8f8f8f',
  white: '#f7f7f7',
  black: '#141414',
  blue: '#0c41ef',
  green: '#16eb14',
  red: '#eb1414',
  orange: '#eba614',
};

const GlobalStyle = createGlobalStyle`
  body {
    font-size: ${props => props.theme.fontSize};
    color: ${props => props.theme.black};
    width: ${props => props.theme.contSize};
    margin: 0 auto;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #C17376;
    font-family: 'Fira Code', monospace;
  }

  #root{
    display: flex;
    margin-top: 10px;
  }

  p, h1, h2, h3, h4, h5, span{
    margin: 0;
    padding: 0;
  }

  h2, h1 {
    color: #6CFDB0;
    text-align: center;
    margin: 5px 0;
  }
  h4 {
    color: #6CFDB0;
    font-size: 14px;
    margin-bottom: 25px;
  }
  h2 {
    font-size: 25px;
    line-height: 36px;
  }
`;

const RightSidebar = styled.div`
  display: flex;
  flex-direction: column;
  height: auto;
  text-align: center;
  background-color: rgba(0, 0, 0, 0.5);
  box-shadow: 0 2px 20px 0 rgba(0, 0, 0, 0.5);
`;

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <Fragment>
          <GlobalStyle />
          <Backgrounds />
          <Editor stageWidth={400} stageHeight={400} />
          <RightSidebar>
            <h2>Enhancements</h2>
            <h4>add some magic</h4>
            <LogoPicker />
            <Text />
            <Controls />
          </RightSidebar>
        </Fragment>
      </ThemeProvider>
    );
  }
}

export default App;
