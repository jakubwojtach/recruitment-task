import axios from 'axios';
import { access_key } from '../Constants/credentials';

export function getRandomImages() {
  return axios({
    method: 'GET',
    url: 'https://api.unsplash.com/photos/random',
    headers: {
      Authorization: `Client-ID ${access_key}`,
    },
    params: {
      count: '4',
      orientation: 'squarish',
    },
  });
}

export function searchForImages(query) {
  return axios({
    method: 'GET',
    url: 'https://api.unsplash.com/photos/search',
    headers: {
      Authorization: `Client-ID ${access_key}`,
    },
    params: {
      query,
      orientation: 'squarish',
      page: 1,
      per_page: 4,
    },
  });
}