import { combineReducers } from 'redux';
import backgrounds from './Backgrounds';
import editor from './Editor';
import text from './Text';
import logos from './Logos';
import undoable from 'redux-undo';

export default combineReducers({
  backgrounds: backgrounds,
  editor: undoable(editor),
  logos: undoable(logos),
  text: undoable(text),
});
