import {
  FETCH_IMAGES_REQUEST,
  FETCH_IMAGES_SUCCESS,
  FETCH_IMAGES_ERROR,
  SEARCH_IMAGES_REQUEST,
  SEARCH_IMAGES_SUCCESS,
  SEARCH_IMAGES_ERROR,
  SET_BACKGROUND_SEARCH_QUERY,
} from '../Constants/actions';

const initialState = {
  images: [],
  error: null,
  searchQuery: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_IMAGES_REQUEST:
    case SEARCH_IMAGES_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case FETCH_IMAGES_SUCCESS:
    case SEARCH_IMAGES_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        error: null,
        images: action.payload,
      };
    }

    case FETCH_IMAGES_ERROR:
    case SEARCH_IMAGES_ERROR: {
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    }

    case SET_BACKGROUND_SEARCH_QUERY: {
      return {
        ...state,
        searchQuery: action.payload,
      };
    }

    default: {
      return state;
    }
  }
};
