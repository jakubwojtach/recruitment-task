import {
  PICK_BACKGROUND_IMAGE,
  CLEAR_BACKGROUND_IMAGE,
  SET_BACKGROUND_TEXT,
  CLEAR_BACKGROUND_TEXT,
  SET_TEXT_FONT,
  CLEAR_STATE,
} from '../Constants/actions';

const initialState = {
  imageUrl: '',
  enteredText: {
    text: '',
    attributes: {
      x: 0,
      y: 0,
    },
  },
  pickedLogo: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case PICK_BACKGROUND_IMAGE: {
      return {
        ...state,
        imageUrl: action.payload,
      };
    }

    case SET_BACKGROUND_TEXT: {
      return {
        ...state,
        enteredText: action.payload,
      };
    }

    case CLEAR_BACKGROUND_IMAGE: {
      return {
        ...state,
        imageUrl: '',
      };
    }

    case CLEAR_BACKGROUND_TEXT: {
      return {
        ...state,
        enteredText: initialState.enteredText,
      };
    }

    case SET_TEXT_FONT: {
      return {
        ...state,
        pickedFont: action.payload,
      };
    }

    case CLEAR_STATE: {
      return {
        ...initialState,
      };
    }

    default: {
      return state;
    }
  }
};
