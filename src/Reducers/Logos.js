import {
  ADD_LOGO,
  REMOVE_LOGO,
  UPDATE_LOGO,
  CLEAR_STATE,
} from '../Constants/actions';

const initialState = {
  list: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_LOGO: {
      return {
        ...state,
        list: state.list ? [...state.list, action.payload] : [action.payload],
      };
    }
    case UPDATE_LOGO: {
      return {
        ...state,
        list: state.list.map(item =>
          item.attributes.name === action.payload.data.attributes.name
            ? action.payload.data
            : item,
        ),
      };
    }
    case REMOVE_LOGO: {
      return {
        ...state,
        list: state.list.filter(el => el.attributes.name !== action.payload),
      };
    }

    case CLEAR_STATE: {
      return {
        ...initialState,
      };
    }

    default: {
      return state;
    }
  }
};
