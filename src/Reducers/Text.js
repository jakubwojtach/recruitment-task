import {
  CHANGE_TEXT_BOLD,
  CHANGE_TEXT_UNDERLINE,
  CHANGE_TEXT_ITALIC,
  SET_TEXT_FONT,
  SET_TEXT_COLOR,
  CLEAR_BACKGROUND_TEXT,
  SET_TEXT_VALUE,
  CLEAR_STATE,
} from '../Constants/actions';

const initialState = {
  fontBold: false,
  fontItalic: false,
  fontUnderline: false,
  fontFamily: 'Arial',
  fontColor: '#000000',
  text: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_TEXT_COLOR: {
      return {
        ...state,
        fontColor: action.payload,
      };
    }

    case CHANGE_TEXT_BOLD: {
      return {
        ...state,
        fontBold: !state.fontBold,
      };
    }

    case CHANGE_TEXT_ITALIC: {
      return {
        ...state,
        fontItalic: !state.fontItalic,
      };
    }

    case CHANGE_TEXT_UNDERLINE: {
      return {
        ...state,
        fontUnderline: !state.fontUnderline,
      };
    }

    case SET_TEXT_FONT: {
      return {
        ...state,
        fontFamily: action.payload,
      };
    }

    case CLEAR_BACKGROUND_TEXT: {
      return {
        ...initialState,
      };
    }

    case SET_TEXT_VALUE: {
      return {
        ...state,
        text: action.payload,
      };
    }

    case CLEAR_STATE: {
      return {
        ...initialState,
      };
    }

    default: {
      return state;
    }
  }
};
