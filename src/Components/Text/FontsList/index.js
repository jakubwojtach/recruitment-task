import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { setTextFont } from '../../../Actions/Text';
import { connect } from 'react-redux';
import styled from 'styled-components';

const FontsContainer = styled.div`
  margin: 10px 0;
  padding: 0 5px;
  display: flex;
  flex-direction: column;
  text-align: left;
  color: #fff;
`;

const ArialInput = styled.label`
  font-family: 'Arial';
`;

const TimesNewRomanInput = styled.label`
  font-family: 'Times New Roman';
`;

const OpenSansInput = styled.label`
  font-family: 'Open Sans';
`;

class FontsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fontFamily: 'Arial',
    };
    this.onFontChange = this.onFontChange.bind(this);
  }

  onFontChange(event) {
    const { setFont } = this.props;
    setFont(event.target.value);
    this.setState({
      fontFamily: event.target.value,
    });
  }

  render() {
    const { font } = this.props;
    return (
      <FontsContainer>
        <ArialInput>
          <input
            name="font"
            value="Arial"
            type="radio"
            checked={font === 'Arial'}
            onChange={this.onFontChange}
          />
          Arial
        </ArialInput>
        <TimesNewRomanInput>
          <input
            name="font"
            value="Times New Roman"
            type="radio"
            checked={font === 'Times New Roman'}
            onChange={this.onFontChange}
          />
          Times New Roman
        </TimesNewRomanInput>
        <OpenSansInput>
          <input
            name="font"
            value="Open Sans"
            type="radio"
            checked={font === 'Open Sans'}
            onChange={this.onFontChange}
          />
          Open Sans
        </OpenSansInput>
      </FontsContainer>
    );
  }
}

const mapStateToProps = state => ({
  font: state.editor.present.pickedFont,
});

const mapDispatchToProps = dispatch => ({
  setFont: text => dispatch(setTextFont(text)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FontsList);

FontsList.propTypes = {
  font: PropTypes.string,
  setFont: PropTypes.func.isRequired,
};
FontsList.defaultProps = {
  font: 'Arial',
};
