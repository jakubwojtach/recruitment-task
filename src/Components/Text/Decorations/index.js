import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import {
  changeTextBold,
  changeTextUndeline,
  changeTextItalic,
} from '../../../Actions/Text';

const DecorationButtons = styled.div`
  display: flex;
  width: 100%;
  margin: 5px 0;
`;

const DecorationButton = styled.div`
  display: flex;
  width: 20px;
  height: 20px;
  align-items: center;
  justify-content: center;
  text-transform: uppercase;
  color: ${props =>
    props.active === true ? props.theme.white : props.theme.black};
  background-color: ${props =>
    props.active === true ? props.theme.black : props.theme.white};
  margin: 0 5px;
  border: 1px solid ${props => props.theme.black};
  border-radius: 5px;
  cursor: pointer;
`;

const ItalicButton = styled(DecorationButton)`
  font-style: italic;
`;

const BoldButton = styled(DecorationButton)`
  font-weight: bold;
`;

const UnderlineButton = styled(DecorationButton)`
  text-decoration: underline;
`;

class TextDecorations extends Component {
  constructor(props) {
    super(props);
    this.setItemBold = this.setItemBold.bind(this);
    this.setItemItalic = this.setItemItalic.bind(this);
    this.setItemUnderline = this.setItemUnderline.bind(this);
  }

  setItemBold() {
    const { changeBold } = this.props;
    changeBold();
  }

  setItemUnderline() {
    const { changeUnderline } = this.props;
    changeUnderline();
  }

  setItemItalic() {
    const { changeItalic } = this.props;
    changeItalic();
  }

  render() {
    const { fontUnderline, fontBold, fontItalic } = this.props;
    return (
      <DecorationButtons>
        <BoldButton active={fontBold} onClick={this.setItemBold}>
          B
        </BoldButton>
        <ItalicButton active={fontItalic} onClick={this.setItemItalic}>
          I
        </ItalicButton>
        <UnderlineButton active={fontUnderline} onClick={this.setItemUnderline}>
          U
        </UnderlineButton>
      </DecorationButtons>
    );
  }
}

const mapStateToProps = state => ({
  fontBold: state.text.present.fontBold,
  fontItalic: state.text.present.fontItalic,
  fontUnderline: state.text.present.fontUnderline,
});

const mapDispatchToProps = dispatch => ({
  changeBold: () => dispatch(changeTextBold()),
  changeItalic: () => dispatch(changeTextItalic()),
  changeUnderline: () => dispatch(changeTextUndeline()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TextDecorations);

TextDecorations.propTypes = {
  changeBold: PropTypes.func.isRequired,
  changeItalic: PropTypes.func.isRequired,
  changeUnderline: PropTypes.func.isRequired,
  fontBold: PropTypes.bool,
  fontItalic: PropTypes.bool,
  fontUnderline: PropTypes.bool,
};
TextDecorations.defaultProps = {
  fontBold: false,
  fontItalic: false,
  fontUnderline: false,
};
