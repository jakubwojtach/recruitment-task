import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import styled from 'styled-components';
import TextDecorations from './Decorations';
import { setOverlayText, clearOverlayText } from '../../Actions/Editor';
import { setTextColor, setTextValue } from '../../Actions/Text';

import Button from '../Common/Button';
import Input from '../Common/Forms/Input';
import FontsList from './FontsList';

const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 350px;
  margin-top: 30px;
  background-image: linear-gradient(-90deg, #ff3176 0%, #ff7369 100%);
  text-align: center;
  padding: 10px 0 15px;
  margin-bottom: 20px;
  justify-content: center;
`;

const formsyStyles = {
  display: 'flex',
  flexDirection: 'column',
};

const TextEnhancers = styled.div`
  display: flex;
  align-items: center;
`;

const StyledInput = styled.input`
  margin-right: 15px;
`;

class Text extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.setOverlayTextValue = this.setOverlayTextValue.bind(this);
    this.onColorChange = this.onColorChange.bind(this);
    this.setText = this.setText.bind(this);
  }

  setOverlayTextValue(formData) {
    const { enteredText, setText } = this.props;
    const { overlayText } = formData;
    const data = {
      text: overlayText,
      attributes: {
        x: 0,
        y: 0,
        deletePending: false,
      },
    };

    if (overlayText !== enteredText) {
      setText(data);
    }
  }

  onColorChange(event) {
    const { setColor, color } = this.props;
    const { value } = event.currentTarget;
    if (value !== color) {
      setColor(value);
    }
  }

  setText(value) {
    const { setTextToStore } = this.props;
    setTextToStore(value);
  }

  render() {
    const { textValue, color, value } = this.props;
    const textValEmpty = textValue ? textValue.text === '' : false;
    return textValEmpty ? (
      <TextContainer>
        <h2>Add Text</h2>
        <Formsy style={formsyStyles} onSubmit={this.setOverlayTextValue}>
          <Input
            name="overlayText"
            type="text"
            value={value}
            placeholder="My Custom Text Goes Here!"
            onChange={this.setText}
          />
          <FontsList />
          <Button
            padding="10px 20px"
            margin="5px auto"
            text="Add Text"
            type="submit"
          />
        </Formsy>
      </TextContainer>
    ) : (
      <TextContainer>
        <h2>Modify Text</h2>
        <FontsList />
        <TextEnhancers>
          <StyledInput
            type="color"
            value={color}
            onChange={this.onColorChange}
          />
          <TextDecorations />
        </TextEnhancers>
      </TextContainer>
    );
  }
}

const mapStateToProps = state => ({
  textValue: state.editor.present.enteredText,
  value: state.text.present.text,
  color: state.text.present.fontColor,
  bold: state.text.present.fontBold,
  italic: state.text.present.fontItalic,
  underline: state.text.present.fontUnderline,
  family: state.text.present.fontFamily,
});

const mapDispatchToProps = dispatch => ({
  setText: text => dispatch(setOverlayText(text)),
  setColor: color => dispatch(setTextColor(color)),
  removeText: () => dispatch(clearOverlayText()),
  setTextToStore: text => dispatch(setTextValue(text)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Text);

Text.propTypes = {
  bold: PropTypes.bool,
  color: PropTypes.string,
  family: PropTypes.string,
  italic: PropTypes.bool,
  removeText: PropTypes.func.isRequired,
  setColor: PropTypes.func.isRequired,
  setTextToStore: PropTypes.func.isRequired,
  textValue: PropTypes.object,
  underline: PropTypes.bool,
  value: PropTypes.string,
};
Text.defaultProps = {
  bold: false,
  color: '',
  family: '',
  italic: false,
  textValue: {},
  underline: false,
  value: '',
};
