import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Common/Button';
import { ActionCreators } from 'redux-undo';
import { clearState } from '../../Actions/Editor';

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: center;
`;

class Controls extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.undoState = this.undoState.bind(this);
    this.redoState = this.redoState.bind(this);
    this.resetState = this.resetState.bind(this);
  }

  undoState() {
    const { undoChanges } = this.props;
    undoChanges();
  }

  redoState() {
    const { redoChanges } = this.props;
    redoChanges();
  }

  resetState() {
    const { clearChanges } = this.props;
    clearChanges();
  }

  render() {
    return (
      <Fragment>
        <h2>Revert/Reset Changes</h2>
        <ButtonsContainer>
          <Button
            padding="10px 15px"
            margin="5px 20px"
            onClick={this.undoState}
            text="Undo"
          />
          <Button
            padding="10px 15px"
            margin="5px 20px"
            onClick={this.redoState}
            text="Redo"
          />
          <Button
            padding="10px 15px"
            margin="5px 20px"
            onClick={this.resetState}
            text="Reset"
          />
        </ButtonsContainer>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
  clearChanges: () => {
    dispatch(clearState());
    dispatch(ActionCreators.clearHistory());
  },
  undoChanges: () => dispatch(ActionCreators.undo()),
  redoChanges: () => dispatch(ActionCreators.redo()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Controls);

Controls.propTypes = {
  clearChanges: PropTypes.func.isRequired,
  redoChanges: PropTypes.func.isRequired,
  undoChanges: PropTypes.func.isRequired,
};
