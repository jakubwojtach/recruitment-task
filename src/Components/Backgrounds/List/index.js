import React, { Component, Fragment } from 'react';
import Item from '../Item';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchImages } from '../../../Actions/Backgrounds';
import BackgroundSearch from '../Search';

class List extends Component {
  componentDidMount() {
    const { getRandomImages, images } = this.props;
    if (!images || !images.length > 0) {
      getRandomImages().then(() => {
        this.setState({
          loader: false,
        });
      });
    }
  }

  render() {
    const { images } = this.props;
    return (
      <Fragment>
        <BackgroundSearch />
        <div className="backgrounds_list">
          {images.map(item => (
            <Item key={item.id} data={item} />
          ))}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  images: state.backgrounds.images,
});

const mapDispatchToProps = dispatch => ({
  getRandomImages: () => dispatch(fetchImages()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(List);

List.propTypes = {
  images: PropTypes.array,
  getRandomImages: PropTypes.func.isRequired,
};
List.defaultProps = {
  images: [],
};
