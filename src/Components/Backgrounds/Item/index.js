import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { setBackgroundUrl } from '../../../Actions/Editor';

const BgItem = styled.div`
  width: ${props => props.imageSize || '150px'};
  height: ${props => props.imageSize || '150px'};
  transition: all ${props => props.transitionTime || '300ms'} ease;
  background-image: url(${props => props.imageUrl});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  border: 3px solid ${props => props.theme.orange};
  margin: 10px 0;
  opacity: ${props => (props.active ? '0.6' : '1')};
  cursor: pointer;

  &:first-of-type {
    margin-top: 0;
  }
  &:hover {
    opacity: 0.6;
  }
`;

class Item extends Component {
  constructor(props) {
    super(props);
    this.selectElement = this.selectElement.bind(this);
  }

  selectElement() {
    const { data, setBgUrl, currentUrl } = this.props;
    if (currentUrl === '' || currentUrl !== data.urls.regular) {
      setBgUrl(data.urls.regular);
    }
  }

  render() {
    const { data, currentUrl } = this.props;
    return (
      <BgItem
        active={data.urls.regular === currentUrl}
        imageUrl={data.urls.thumb}
        onClick={this.selectElement}
      />
    );
  }
}

const mapStateToProps = state => ({
  currentUrl: state.editor.present.imageUrl,
});

const mapDispatchToProps = dispatch => ({
  setBgUrl: url => dispatch(setBackgroundUrl(url)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Item);

Item.propTypes = {
  data: PropTypes.object.isRequired,
  setBgUrl: PropTypes.func.isRequired,
  currentUrl: PropTypes.string,
};
Item.defaultProps = {
  currentUrl: '',
};
