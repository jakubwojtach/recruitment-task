import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import List from './List';
import Button from '../Common/Button';
import styled from 'styled-components';

import { clearBackgroundUrl } from '../../Actions/Editor';

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 300px;
  align-items: center;
  padding: 10px 0;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  box-shadow: 0 2px 20px 0 rgba(0, 0, 0, 0.5);
`;

class Backgrounds extends Component {
  constructor(props) {
    super(props);
    this.removeSelectedBackground = this.removeSelectedBackground.bind(this);
  }

  removeSelectedBackground() {
    const { clearBg, currentUrl } = this.props;
    if (currentUrl !== '') {
      clearBg();
    }
  }

  render() {
    const { currentUrl } = this.props;
    return (
      <StyledWrapper>
        <h2>Select Background</h2>
        <h4>pick something nice</h4>
        <List />
        {currentUrl !== '' ? (
          <Button
            padding="10px 20px"
            margin="5px auto"
            onClick={this.removeSelectedBackground}
            text="Delete Background"
          />
        ) : null}
      </StyledWrapper>
    );
  }
}

const mapStateToProps = state => ({
  images: state.backgrounds.images,
  currentUrl: state.editor.present.imageUrl,
});

const mapDispatchToProps = dispatch => ({
  clearBg: () => dispatch(clearBackgroundUrl()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Backgrounds);

Backgrounds.propTypes = {
  clearBg: PropTypes.func.isRequired,
  currentUrl: PropTypes.string,
  images: PropTypes.array,
};
Backgrounds.defaultProps = {
  currentUrl: '',
  images: [],
};
