import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Formsy from 'formsy-react';

import Input from '../../Common/Forms/Input';
import { clearBackgroundUrl } from '../../../Actions/Editor';
import { fetchImagesByQuery, setQuery } from '../../../Actions/Backgrounds';

class BackgroundSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      typing: false,
      typingTimeout: 0,
    };
    this.searchImagesCall = this.searchImagesCall.bind(this);
    this.setValueToStore = this.setValueToStore.bind(this);
  }

  setValueToStore(query) {
    const { saveQueryToStore } = this.props;
    saveQueryToStore(query);
  }

  searchImagesCall(query) {
    const { currentUrl, clearBg, searchImages } = this.props;
    if (currentUrl !== '') {
      clearBg();
    }
    searchImages(query);
  }

  render() {
    const { query } = this.props;
    const formsyStyles = {
      display: 'flex',
      flexDirection: 'column',
      marginBottom: '10px',
    };
    return (
      <Formsy style={formsyStyles} ref={form => (this.form = form)}>
        <Input
          name="query"
          type="text"
          value={query}
          required
          placeholder="Start typing to find images.."
          onChange={this.setValueToStore}
          debouncedCall={this.searchImagesCall}
        />
      </Formsy>
    );
  }
}

const mapStateToProps = state => ({
  currentUrl: state.editor.imageUrl,
  query: state.backgrounds.searchQuery,
});

const mapDispatchToProps = dispatch => ({
  clearBg: () => dispatch(clearBackgroundUrl()),
  searchImages: query => dispatch(fetchImagesByQuery(query)),
  saveQueryToStore: query => dispatch(setQuery(query)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BackgroundSearch);

BackgroundSearch.propTypes = {
  clearBg: PropTypes.func.isRequired,
  query: PropTypes.string,
  saveQueryToStore: PropTypes.func.isRequired,
  searchImages: PropTypes.func.isRequired,
};
BackgroundSearch.defaultProps = {
  query: '',
};
