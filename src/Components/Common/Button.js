import styled from 'styled-components';
import React from 'react';
import PropTypes from 'prop-types';

const StyledButton = styled.button`
  padding: ${props => props.padding};
  margin: ${props => props.margin};
  background-color: ${props => props.theme.orange};
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  font-family: 'Fira Code';
  width: ${props => props.width || 'auto'};
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
  color: ${props => props.theme.white};
  font-size: ${props => props.fontSize || '16px'};
  cursor: pointer;
  &:hover {
    box-shadow: 0 5px 15px rgba(0, 0, 0, 0.25), 0 5px 10px rgba(0, 0, 0, 0.22);
  }
  border: 0;
  text-transform: uppercase;
`;

const Button = ({ padding, margin, onClick, text, type }) => (
  <StyledButton
    padding={padding}
    margin={margin}
    onClick={onClick || undefined}
    type={type || 'button'}
  >
    {text}
  </StyledButton>
);

Button.propTypes = {
  margin: PropTypes.string,
  padding: PropTypes.string,
  text: PropTypes.string,
  type: PropTypes.string,
};

Button.defaultProps = {
  margin: '',
  padding: '',
  text: '',
  type: '',
};
export default Button;
