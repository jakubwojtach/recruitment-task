import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { withFormsy } from 'formsy-react';
import { debounce } from '../../../helpers';


const StyledInput = styled.input`
  width: 200px;
  height: 35px;
  border: 1px solid ${props => props.theme.black};
  color: ${props => props.theme.black};
  margin: 5px 0;
  padding: 0 5px;
  &:focus {
    border-color: ${props => props.theme.blue};
    outline: none;
  }
`;

class Input extends Component {
  constructor(props) {
    super(props);
    this.changeValue = this.changeValue.bind(this);
    this.debouncedCallFunc = this.debouncedCallFunc.bind(this);
  }

  changeValue(event) {
    this.props.setValue(event.currentTarget.value);
    if (this.props.onChange) {
      this.props.onChange(event.currentTarget.value);
    }
  }

  debouncedCallFunc() {
    const { getValue, debouncedCall } = this.props;
    const value = getValue();
    debouncedCall(value);
  }

  render() {
    const {
      getErrorMessage,
      name,
      required,
      type,
      placeholder,
      value,
      debouncedCall
    } = this.props;
    const errorMessage = getErrorMessage();
    return (
      <Fragment>
        <StyledInput
          onChange={(e) => {
            this.changeValue(e);
          }}
          onKeyPress={(e) => {
            if (debouncedCall) {
              debounce(this.debouncedCallFunc, 500);
            }}
          }
          name={name}
          value={value}
          required={required}
          type={type || 'text'}
          placeholder={placeholder || ''}
        />
        <span>{errorMessage}</span>
      </Fragment>
    );
  }
}

Input.propTypes = {
  getErrorMessage: PropTypes.func.isRequired,
  name: PropTypes.string,
  required: PropTypes.bool,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
};

Input.defaultProps = {
  name: '',
  required: false,
  type: '',
  placeholder: '',
  value: '',
};

export default withFormsy(Input);
