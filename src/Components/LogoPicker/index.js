import React, { Component } from 'react';
import Item from './Item';
import styled from 'styled-components';

const Wrapper = styled.div`
  background-image: linear-gradient(-90deg, #ff3176 0%, #ff7369 100%);
  display: flex;
  flex-direction: column;
  text-align: center;
  padding: 10px 0 15px;
`;

const Container = styled.div`
  display: flex;
  justify-content: center;
`;
export default class LogoPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Wrapper>
        <h2>Add Logo</h2>
        <Container>
          <Item source="/img/logo_one.png" />
          <Item source="/img/logo_two.png" />
          <Item source="/img/logo_three.png" />
        </Container>
      </Wrapper>
    );
  }
}
