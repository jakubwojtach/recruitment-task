import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { addLogo } from '../../../Actions/Logo';

const StyledImage = styled.img`
  width: 70px;
  height: 70px;
  margin: 0 10px;
  transition: all 300ms ease;
  cursor: pointer;
  &:hover {
    transform: rotate(15deg);
  }
`;

class LogoItem extends Component {
  constructor(props) {
    super(props);
    this.addItem = this.addItem.bind(this);
  }

  addItem() {
    const { source, addLogoToList } = this.props;
    const object = {
      url: source,
      attributes: {
        deletePending: false,
        width: 100,
        height: 100,
        scaleX: 1,
        scaleY: 1,
        name: String(Date.now()),
      },
    };
    addLogoToList(object);
  }

  render() {
    const { source } = this.props;
    return <StyledImage src={source} onClick={this.addItem} />;
  }
}

const mapStateToProps = state => ({
  logos: state.logos.present.list,
});

const mapDispatchToProps = dispatch => ({
  addLogoToList: data => dispatch(addLogo(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LogoItem);

LogoItem.propTypes = {
  addLogoToList: PropTypes.func.isRequired,
  logos: PropTypes.array,
  source: PropTypes.string.isRequired,
};
LogoItem.defaultProps = {
  logos: [],
};
