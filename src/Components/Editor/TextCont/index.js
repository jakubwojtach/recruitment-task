import React, { Component, Fragment } from 'react';
import { Text } from 'react-konva';
import ContextMenu from '../ContextMenu';

class TextContainer extends Component {
  render() {
    const {
      textValue,
      underline,
      bold,
      italic,
      deletePending,
      deleteFunction,
      type,
    } = this.props;
    return (
      <Fragment>
        <Text
          textDecoration={underline ? 'underline' : ''}
          text={textValue}
          fontStyle={`${bold ? 'bold' : ''} ${italic ? 'italic' : ''}`}
          {...this.props}
        />
        {deletePending === true ? (
          <ContextMenu
            removeElement={deleteFunction}
            type={type}
            name=""
            x={
              this.props.type === 'TextNode'
                ? this.props.x - 20
                : this.props.x - this.props.width
            }
            y={
              this.props.type === 'TextNode'
                ? this.props.y + 20
                : this.props.y - this.props.height
            }
          />
        ) : null}
      </Fragment>
    );
  }
}

export default TextContainer;
