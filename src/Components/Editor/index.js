import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import TextContainer from './TextCont';
import Button from '../Common/Button';
import { Stage, Layer } from 'react-konva';
import BackgroundImage from './BGCont';
import CanvasImg from './ImageCont';
import { updateLogo, removeLogo } from '../../Actions/Logo';
import { setOverlayText, clearOverlayText } from '../../Actions/Editor';
import { setTextValue } from '../../Actions/Text';
import Resizer from './Resizer';
import PromoLogo from '../Common/PromoLogo';

const EditorWrapper = styled.div`
  width: 550px;
  height: auto;
  margin: 0 25px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  align-items: center;
  padding: 10px 0;
  background-color: rgba(0, 0, 0, 0.5);
  box-shadow: 0 2px 20px 0 rgba(0, 0, 0, 0.5);
`;

class Editor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedShapeName: '',
    };
    this.prepareFinalImage = this.prepareFinalImage.bind(this);
    this.updateElement = this.updateElement.bind(this);
    this.handleContextMenu = this.handleContextMenu.bind(this);
    this.handleStageMouseDown = this.handleStageMouseDown.bind(this);
    this.removeElement = this.removeElement.bind(this);
  }

  prepareFinalImage() {
    const downloadURI = (uri, name) => {
      const link = document.createElement('a');
      link.download = name;
      link.href = uri;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    };
    this.setState(
      {
        selectedShapeName: '',
      },
      () =>
        downloadURI(
          this.canvas.toDataURL({ pixelRatio: 3 }),
          'exportedImage.png',
        ),
    );
  }

  updateElement(e, type, ...rest) {
    const { attrs } = e.target;
    const { updateLogoData, updateText } = this.props;
    let object = {};
    switch (type) {
      case 'textNode': {
        object = {
          text: rest[0],
          attributes: attrs,
        };
        updateText(object);
        break;
      }
      case 'logoNode': {
        object = {
          data: {
            url: rest[0],
            attributes: attrs,
          },
        };
        updateLogoData(object);
        break;
      }
      default: {
        break;
      }
    }
  }

  handleContextMenu(e, element, type) {
    e.evt.preventDefault();
    const { updateLogoData, updateText } = this.props;

    if (type === 'logoNode') {
      const object = {
        name: element.attributes.name,
        data: {
          ...element,
          attributes: {
            ...element.attributes,
            deletePending: !element.attributes.deletePending,
          },
        },
      };
      updateLogoData(object);
    } else {
      const object = {
        ...element,
        attributes: {
          ...element.attributes,
          width: e.target.textWidth,
          height: e.target.textHeight,
          deletePending: !element.attributes.deletePending,
        },
      };
      updateText(object);
    }
  }

  removeElement(type, name = '') {
    const { removeLogoElement, removeText, setTextToStore } = this.props;
    if (type === 'LogoNode') {
      removeLogoElement(name);
    } else {
      removeText();
      setTextToStore('');
    }
  }

  handleStageMouseDown(e) {
    if (e.target === e.target.getStage()) {
      this.setState({
        selectedShapeName: '',
        selectedShape: null,
      });
      return;
    }

    if (e.target.getParent().className === 'Transformer') {
      return;
    }

    const name = e.target.attrs.name;
    if (name) {
      this.setState({
        selectedShapeName: name,
        selectedShape: e.target,
      });
    } else {
      this.setState({
        selectedShapeName: '',
        selectedShape: null,
      });
    }
  }

  render() {
    const {
      textObject,
      bgUrl,
      color,
      family,
      bold,
      underline,
      italic,
      logos,
      stageWidth,
      stageHeight,
    } = this.props;
    return (
      <EditorWrapper>
        <PromoLogo />
        <h2>Simple Editor</h2>
        <h4>by Jakub Wojtach</h4>
        <Stage
          width={stageWidth}
          height={stageHeight}
          ref={canvas => (this.canvas = canvas)}
          onMouseDown={this.handleStageMouseDown}
        >
          <Layer>
            <BackgroundImage
              width={stageWidth}
              height={stageHeight}
              bgUrl={bgUrl}
            />
            {textObject.text !== '' ? (
              <TextContainer
                textValue={textObject.text || ''}
                fontSize={20}
                fill={color}
                fontFamily={family}
                deletePending={textObject.attributes.deletePending}
                bold={bold}
                italic={italic}
                underline={underline}
                type="TextNode"
                width={stageWidth - 20}
                height={stageHeight}
                wrap="word"
                deleteFunction={this.removeElement}
                x={textObject.attributes.x || 0}
                y={textObject.attributes.y || 0}
                draggable
                onDragEnd={e =>
                  this.updateElement(e, 'textNode', textObject.text)
                }
                dragBoundFunc={function(pos) {
                  const maxXPosition =
                    pos.x > stageWidth - this.textWidth
                      ? stageWidth - this.textWidth
                      : pos.x;
                  const maxYPosition =
                    pos.y > stageHeight - this.textHeight * this.textArr.length
                      ? stageHeight - this.textHeight * this.textArr.length
                      : pos.y;
                  return {
                    x: maxXPosition > 0 ? maxXPosition : 0,
                    y: maxYPosition > 0 ? maxYPosition : 0,
                  };
                }}
                onContextMenu={e =>
                  this.handleContextMenu(e, textObject, 'textNode')
                }
              />
            ) : null}
            {logos
              ? logos.map(logo => (
                  <CanvasImg
                    width={logo.attributes.width}
                    height={logo.attributes.height}
                    bgUrl={logo.url}
                    x={logo.attributes.x}
                    y={logo.attributes.y}
                    scaleX={logo.attributes.scaleX}
                    scaleY={logo.attributes.scaleY}
                    name={logo.attributes.name}
                    type="LogoNode"
                    deletePending={logo.attributes.deletePending}
                    draggable
                    deleteFunction={this.removeElement}
                    key={`logo-${logo.attributes.name}`}
                    onDragEnd={e => this.updateElement(e, 'logoNode', logo.url)}
                    onTransformEnd={e =>
                      this.updateElement(e, 'logoNode', logo.url)
                    }
                    onContextMenu={e =>
                      this.handleContextMenu(e, logo, 'logoNode')
                    }
                    dragBoundFunc={pos => {
                      const maxXPosition =
                        pos.x >
                        stageWidth -
                          logo.attributes.width * logo.attributes.scaleX
                          ? stageWidth -
                            logo.attributes.width * logo.attributes.scaleX
                          : pos.x;
                      const maxYPosition =
                        pos.y >
                        stageHeight -
                          logo.attributes.height * logo.attributes.scaleY
                          ? stageHeight -
                            logo.attributes.height * logo.attributes.scaleY
                          : pos.y;
                      return {
                        x: maxXPosition > 0 ? maxXPosition : 0,
                        y: maxYPosition > 0 ? maxYPosition : 0,
                      };
                    }}
                  />
                ))
              : null}
            <Resizer
              minWidth={30}
              minHeight={30}
              maxWidth={150}
              maxHeight={150}
              selectedElement={this.state.selectedShapeName}
              selectedShape={this.state.selectedShape}
              stageWidth={stageWidth}
              stageHeight={stageHeight}
            />
          </Layer>
        </Stage>
        {(textObject && textObject.text !== '') ||
        bgUrl !== '' ||
        logos.length > 0 ? (
          <Button
            padding="15px 50px"
            margin="25px auto"
            width="100%"
            fontSize="30px"
            onClick={this.prepareFinalImage}
            text="Download Image"
          />
        ) : null}
      </EditorWrapper>
    );
  }
}

const mapStateToProps = state => ({
  bgUrl: state.editor.present.imageUrl,
  textObject: state.editor.present.enteredText,
  color: state.text.present.fontColor,
  bold: state.text.present.fontBold,
  italic: state.text.present.fontItalic,
  underline: state.text.present.fontUnderline,
  family: state.text.present.fontFamily,
  logos: state.logos.present.list,
});

const mapDispatchToProps = dispatch => ({
  updateLogoData: data => dispatch(updateLogo(data)),
  updateText: data => dispatch(setOverlayText(data)),
  removeText: () => dispatch(clearOverlayText()),
  removeLogoElement: name => dispatch(removeLogo(name)),
  setTextToStore: text => dispatch(setTextValue(text)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Editor);

Editor.propTypes = {
  bgUrl: PropTypes.string,
  bold: PropTypes.bool,
  color: PropTypes.string,
  family: PropTypes.string,
  italic: PropTypes.bool,
  logos: PropTypes.array,
  removeLogoElement: PropTypes.func.isRequired,
  setTextToStore: PropTypes.func.isRequired,
  removeText: PropTypes.func.isRequired,
  stageHeight: PropTypes.number,
  stageWidth: PropTypes.number,
  textObject: PropTypes.object,
  underline: PropTypes.bool,
  updateLogoData: PropTypes.func.isRequired,
  updateText: PropTypes.func.isRequired,
};
Editor.defaultProps = {};
