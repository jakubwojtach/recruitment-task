import React, { Component } from 'react';
import { Rect, Text } from 'react-konva';

export default class ContextMenu extends Component {
  removeData() {
    const { removeElement, type, name } = this.props;
    removeElement(type, name);
  }

  render() {
    const { x, y } = this.props;
    return [
      <Rect
        key="context-menu-bg"
        x={x}
        y={y}
        width={80}
        height={20}
        fill="#cccccc"
        onClick={() => this.removeData()}
      />,
      <Text
        key="context-menu-text"
        x={x + 20}
        y={y + 5}
        text="Delete"
        fontSize={12}
        onClick={() => this.removeData()}
      />,
    ];
  }
}
