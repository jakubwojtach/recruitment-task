import React, { Component, Fragment } from 'react';
import { Image } from 'react-konva';
import ContextMenu from '../ContextMenu';

export default class CanvasImg extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
    };
  }

  componentDidMount() {
    this.refreshImage();
  }

  componentDidUpdate(prevImage) {
    if (prevImage.bgUrl !== this.props.bgUrl) {
      this.refreshImage();
    }
  }

  refreshImage() {
    let bgUrl = this.props.bgUrl;
    if (bgUrl === '') {
      bgUrl = '';
    }
    const image = new window.Image();
    image.src = bgUrl;
    image.onload = () => {
      this.setState({
        image,
      });
    };
  }

  render() {
    const { deletePending, deleteFunction, type, name = '' } = this.props;
    return (
      <Fragment>
        <Image {...this.props} image={this.state.image} />
        {deletePending === true ? (
          <ContextMenu
            removeElement={deleteFunction}
            type={type}
            name={name}
            x={this.props.x + this.props.scaleX * this.props.width}
            y={this.props.y + this.props.scaleY * this.props.height}
          />
        ) : null}
      </Fragment>
    );
  }
}
