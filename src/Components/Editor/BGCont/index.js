import React, { Component } from 'react';
import { Image } from 'react-konva';

export default class BackgroundImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
    };
  }

  componentDidMount() {
    this.refreshImage();
  }

  componentDidUpdate(prevImage) {
    if (prevImage.bgUrl !== this.props.bgUrl) {
      this.refreshImage();
    }
  }

  refreshImage() {
    let bgUrl = this.props.bgUrl;
    if (bgUrl === '') {
      bgUrl = '/img/empty_background.bmp';
    }
    const image = new window.Image();
    image.src = bgUrl;
    image.crossOrigin = 'Anonymous';
    image.onload = () => {
      this.setState({
        image,
      });
    };
  }

  render() {
    return <Image {...this.props} image={this.state.image} />;
  }
}
