import React, { Component } from 'react';
import { Transformer } from 'react-konva';

export default class Resizer extends Component {
  componentDidMount() {
    this.checkNode();
  }

  componentDidUpdate() {
    this.checkNode();
  }

  checkNode() {
    const stage = this.transformer.getStage();
    const { selectedElement } = this.props;
    const node = stage.findOne(`.${selectedElement}`);

    if (node === this.transformer.node()) {
      return;
    }

    if (node) {
      this.transformer.attachTo(node);
    } else {
      this.transformer.detach();
    }
    this.transformer.getLayer().batchDraw();
  }

  render() {
    const { maxWidth, minWidth, maxHeight, minHeight, selectedShape, stageWidth, stageHeight } = this.props;
    return (
      <Transformer
        keepRatio={true}
        enabledAnchors={['top-left', 'bottom-right']}
        rotateEnabled={false}
        ref={transformer => (this.transformer = transformer)}
        boundBoxFunc={function(oldBoundBox, newBoundBox) {
          const maxXPosition = stageWidth - selectedShape.attrs.width * selectedShape.attrs.scaleX;
          const maxYPosition = stageHeight - selectedShape.attrs.height * selectedShape.attrs.scaleY;
          if (selectedShape.attrs.x > maxXPosition || selectedShape.attrs.y > maxYPosition) {
            this.stopTransform();
          }

          if (selectedShape.attrs.x < 0 || selectedShape.attrs.y < 0) {
            this.stopTransform();
          }
              
          if (newBoundBox.width > maxWidth) {
            newBoundBox.width = maxWidth;
          }
          if (newBoundBox.height > maxHeight) {
            newBoundBox.height = maxHeight;
          }
          if (newBoundBox.width < minWidth) {
            newBoundBox.width = minWidth;
          }
          if (newBoundBox.height < minHeight) {
            newBoundBox.height = minHeight;
          }

          return newBoundBox;
        }}
      />
    );
  }
}
