let timer;

export function debounce(callback, timeout) {
    clearTimeout(timer);
    timer = setTimeout(() => {
        callback();
    }, timeout)
}
